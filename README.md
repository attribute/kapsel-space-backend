# kapsel.space backend

This repository contains the backend for [kapsel.space](https://kapsel.space/). It is built using [Drupal](https://www.drupal.org/) and provides data using jsonapi for [kapsel-space-backend](https://gitlab.com/attribute/kapsel-space-backend), the frontend based on [Nuxt.js](https://nuxtjs.org) and [Vue.js](https://vuejs.org/).

## Requirements
* [Docker](https://docs.docker.com/engine/install/)
* [Docker Compose](https://docs.docker.com/compose/install/)
* [pgymy](https://lagoon.readthedocs.io/en/latest/using_lagoon/local_development_environments/)

## Installation

``` bash
# Start docker container
$ docker-compose up -d

# ssh into the docker container
$ docker-compose exec cli bash

# Install dependencies
$ composer install

# Install Drupal
$ drush si --existing-config minimal

# Generate login link
$ drush uli
```
Now you should have a running install of the backend at [http://kapsel.space.docker.amazee.io/](http://gds-fm.docker.amazee.io/)
