#!/usr/bin/env bash

set -ve

npm install -g bower
bower --allow-root install
