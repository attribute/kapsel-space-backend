#!/usr/bin/env bash

set -ve

for theme in $THEME_NAMES; do
  if [ "$theme" != false ] ; then
    cd /app/web/themes/${theme}
    ( IFS=:
      for deployment in $THEME_DEPLOYMENTS; do
        if [ "$deployment" != false ] ; then
          eval "/app/scripts/frontend/$deployment.sh"
        fi
      done
    )
  fi
done

/app/scripts/frontend/custom.sh
