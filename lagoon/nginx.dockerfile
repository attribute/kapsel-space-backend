ARG CLI_IMAGE
FROM ${CLI_IMAGE} as cli

FROM amazeeio/nginx-drupal

COPY --from=cli /app /app

ENV DOMAIN=backend.kapsel.space
ENV ESCAPED_DOMAIN='backend\.kapsel.space'
ENV WWW_REDIRECT=non-www

# Add redirects
COPY lagoon/redirects-map.conf /etc/nginx/redirects-map.conf

RUN if [ ${WWW_REDIRECT} == "www" ]; \
      then echo "~^${ESCAPED_DOMAIN} https://www.backend.kapsel.space\$request_uri;" >> /etc/nginx/redirects-map.conf ; \
      else echo "~^www\.${ESCAPED_DOMAIN} https://backend.kapsel.space\$request_uri;" >> /etc/nginx/redirects-map.conf ; \
    fi

# Define where the Drupal Root is located
ENV WEBROOT=web
