FROM amazeeio/php:8.1-cli-drupal
COPY scripts /app/scripts

ENV NODE_VERSION=

RUN /app/scripts/docker/install-node.sh
# RUN /app/scripts/docker/install-packages.sh

ENV THEME_NAMES=false
ENV THEME_DEPLOYMENTS=false

COPY composer.json composer.lock /app/
COPY scripts /app/scripts
COPY patches /app/patches
RUN time composer self-update --2
RUN time composer install --no-dev --apcu-autoloader --optimize-autoloader --prefer-dist

COPY . /app

RUN /app/scripts/frontend/build.sh

# Define where the Drupal Root is located
ENV WEBROOT=web
