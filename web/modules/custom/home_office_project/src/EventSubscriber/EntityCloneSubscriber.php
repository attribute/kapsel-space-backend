<?php

namespace Drupal\home_office_project\EventSubscriber;

use Drupal\commerce_order\OrderTotalSummaryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\entity_clone\Event\EntityCloneEvent;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Sends an email when the order transitions to Fulfillment.
 */
class EntityCloneSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'entity_clone.pre_clone' => 'handlePreClone',
      'entity_clone.post_clone' => 'handlePostClone',
    ];
    return $events;
  }

  /**
   * Modifies entity values before cloning
   *
   * @param \Drupal\entity_clone\Event\EntityCloneEvent $event
   *   The  event.
   */
  public function handlePreClone(EntityCloneEvent $event) {
    /** @var EntityInterface $cloned_entity */
    $cloned_entity = $event->getClonedEntity();
    $cloned_entity->set('field_embed_code', []);
    $cloned_entity->set('status', 0);
  }

  /**
   * Redirects after cloning
   *
   * @param \Drupal\entity_clone\Event\EntityCloneEvent $event
   *   The  event.
   */
  public function handlePostClone(EntityCloneEvent $event) {
    \Drupal::request()->query->remove('destination');
    /** @var EntityInterface $cloned_entity */
    $cloned_entity = $event->getClonedEntity();
    $url = $cloned_entity->toUrl('edit-form', ['absolute' => TRUE]);
    $string = $url->toString();
    $response = new RedirectResponse($string);
    $response->send();
  }

}
